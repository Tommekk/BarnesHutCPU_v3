//============================================================================
// Name        : BarnesHutCPU.cpp
// Author      : Dominik Thomas
// Version     :
// Copyright   : Copyright 2016 Dominik Thomas
// Description : Hello World in C, Ansi-style
//============================================================================

#include <boost/program_options.hpp>


#include <iostream>
#include <cmath>
#include <random>
#include <map>
#include <fstream>
#include <string>
#include <mutex>
#include <chrono>

// TODO add verbose mode

//#define INFO_MODE // Used for infos
#define USE_CPU
#define USE_PARALLEL
#define USE_MINMAX_ELEMENT

#ifdef USE_CPU
#include <vector>
#include <unordered_map>
#include <boost/iterator/counting_iterator.hpp>


// system namespace for datatypes and algorithms (std::pair or thrust::pair etc.)
namespace sys = std;

template <typename T>
using counting_iterator = boost::counting_iterator<T>;

#ifdef USE_PARALLEL
#include <parallel/algorithm>

namespace policy = __gnu_parallel;
#else
#include <algorithm>
#include <queue>

namespace policy = std;
#endif

#endif // TODO else USE GPU

#ifdef USE_GPU
#include <thrust/for_each.h>

namespace sys = thrust;
#endif


//#ifdef USE_PAR_EXECUTION_POLICY
//#define EXECUTION_POLICY std::execution::par
//#else
//#define EXECUTION_POLICY std::execution::seq
//#endif

namespace po = boost::program_options;



namespace BH {



// TODO ifdef USE_GPU

// Datatypes
/*
//                    min_x, max_x, min_y, max_y, min_z, max_z
typedef thrust::tuple<double, double, double, double, double, double> bbox_type; // boundarybox

typedef std::vector<bbox_type> bboxvec_type;
// position vector
//                               x                               y                             z
typedef thrust::tuple<std::vector<double>, std::vector<double>, std::vector<double> > posvec_type;
// velocity vector
//                             v_x                             v_y                           v_z
typedef thrust::tuple<std::vector<double>, std::vector<double>, std::vector<double> > velvec_type;
 */


// TODO alle std::cout auf printfs umstellen

// Build tree


int N=20;//1200; // TODO remove comments
int timesteps=2; // 500
bool use_timer;
// TODO change
//const double G = 6.67408e-11;
const double G = 1;

// TODO replace .at functions by operator[], because then no bounds checkings are active (faster)
// TODO code analyzer
// TODO make viewer

struct Particle {
	double m;
	double x, y, z;
	double v_x, v_y, v_z;
	double a_x, a_y, a_z;
};

struct TreeNode {
	double m_x, m_y, m_z;
	double min_x, min_y, min_z;
	double max_x, max_y, max_z;
	int parentnode;
	sys::pair<int, int> subnodes, particles;
};

typedef sys::vector<TreeNode> TreeNodeVector;
typedef TreeNodeVector:iterator TreeNodeVectorIterator;

sys::vector<Particle> particles(N);
sys::vector<sys::vector<TreeNode>> tree;

// Minimum and maximum coordinates
// TODO remove because they are now implicit in the root node
double min_x, min_y, min_z;
double max_x, max_y, max_z;
double d; // max. needed axis length

void generateRandomEntrys() // TODO replace with thrust tabulate
{
	std::srand(0);
	std::default_random_engine gen(6);
	std::uniform_real_distribution<> dis1(-500000, 500000);
	std::uniform_real_distribution<> dis2(-1, 1);
	std::uniform_real_distribution<> dis3(1000000, 5000000);
	std::uniform_real_distribution<> dis4(-M_PI, M_PI);
	std::uniform_real_distribution<> dis5(-100, 100);
	auto random_pos = [&](){return dis1(gen);};
	auto random_vel = [&](){return dis2(gen);};
	//auto return_zero = [](){return 0;};
	// auto rdis2 = [&](){return dis2(gen);};
	//auto rdis3 = [](){return 3;};//[&](){return dis3(gen);};

	auto rdis6 = [&](){return dis3(gen);};//[&](){return dis3(gen);};

	policy::generate(particles.begin(), particles.end(), [&]{
		//		Particle p = {rdis6(), random_pos(), random_pos(), random_pos(),
		//				random_vel, random_vel, random_vel,
		//				0, 0, 0};
		Particle p;
		p.m = rdis6();
		p.x = random_pos();
		p.y = random_pos();
		p.z = random_pos();
		p.v_x = random_vel();
		p.v_y = random_vel();
		p.v_z = random_vel();
		p.a_x = p.a_y = p.a_z = 0;
		return p;
	});
}



// method to seperate bits from a given integer 3 positions apart
inline uint64_t splitBy3(unsigned int a) {
	uint64_t x = a & 0x1fffff; // we only look at the first 21 bits
	x = (x | x << 32) & 0x1f00000000ffff;  // shift left 32 bits, OR with self, and 00011111000000000000000000000000000000001111111111111111
	x = (x | x << 16) & 0x1f0000ff0000ff;  // shift left 32 bits, OR with self, and 00011111000000000000000011111111000000000000000011111111
	x = (x | x << 8) & 0x100f00f00f00f00f; // shift left 32 bits, OR with self, and 0001000000001111000000001111000000001111000000001111000000000000
	x = (x | x << 4) & 0x10c30c30c30c30c3; // shift left 32 bits, OR with self, and 0001000011000011000011000011000011000011000011000011000100000000
	x = (x | x << 2) & 0x1249249249249249;
	return x;
}


uint64_t convertPositionToMortonCode(const BH::Particle &p) {
	uint64_t answer = 0;
	unsigned int x = ((int64_t) p.x) + (1 << 20);
	unsigned int y = ((int64_t) p.y) + (1 << 20);
	unsigned int z = ((int64_t) p.z) + (1 << 20);

	answer |= splitBy3(x) | splitBy3(y) << 1 | splitBy3(z) << 2;
	// return answer;
	return x;
}

// TODO for auto Schleife benutzen
void printParticleData() {
	for(int i = 0; i < N; i++) 	{
		std::cout << i << ". Morton: (" << convertPositionToMortonCode(particles[i]) << ") Mass:" << particles[i].m;
		std::cout << " {" << particles[i].x << "," << particles[i].y << "," << particles[i].z << "}";
		std::cout << " v{" << particles[i].v_x << "," << particles[i].v_y << "," << particles[i].v_z << "}";
		std::cout << " a{" << particles[i].a_x << "," << particles[i].a_y << "," << particles[i].a_z << "}" << std::endl;
	}
}

// TODO merge two particles with same morton code

// Morton encode with magic bits by Jeroen Baert
void sortParticlesInMortonOrder() {
	policy::sort(particles.begin(), particles.end(), [&](const Particle &a, const Particle &b) {
		uint64_t morton_a = convertPositionToMortonCode(a);
		uint64_t morton_b = convertPositionToMortonCode(b);
		return morton_a < morton_b;
	});
}

// returns true, if contains no particles, but can also be no leave node
bool isEmpty(const TreeNode &treenode) {
	if (treenode.particles.second == 0) {
		return true;
	}
	return false;
}

// returns true, if node has no subnodes
bool isLeave(const TreeNode &treenode) {
	if (treenode.subnodes.second > 0) {
		return true;
	}
	return false;
}

// TODO remove if not used
bool allLeaves(TreeNodeVectorIterator begin,
		TreeNodeVectorIterator end) {
	TreeNodeVectorIterator notLeave = policy::find_if(begin, end, [](const TreeNode &tn) {
		return !isLeave(tn);
	});
	if (notLeave == end)
		return true;
	return false;
}

bool hasSubnodes(const TreeNode &treenode) {
	// has only subnodes if first entry and second differ
	if (treenode.subnodes.first == treenode.subnodes.second) {
		return false;
	}
	return true;
}

bool hasOneParticle(const TreeNode &treenode) {
	// has only subnodes if first entry and second differ
	if (treenode.particles.first == treenode.particles.second-1) {
		return true;
	}
	return false;
}

void buildTree() {
	// TODO delete old tree

	sortParticlesInMortonOrder();


	// TODO remove if not necessary
	////////////////////////////////////////////////
	auto cmp_x = [](const Particle &a, const Particle &b) {
		return a.x < b.x;
	};
	auto cmp_y = [](const Particle &a, const Particle &b) {
		return a.y < b.y;
	};
	auto cmp_z = [](const Particle &a, const Particle &b) {
		return a.z < b.z;
	};

	decltype(particles)::iterator minEl_x, maxEl_x;
	decltype(particles)::iterator minEl_y, maxEl_y;
	decltype(particles)::iterator minEl_z, maxEl_z;


	sys::tie(minEl_x, maxEl_x) = sys::minmax_element(particles.begin(), particles.end(), cmp_x);
	sys::tie(minEl_y, maxEl_y) = sys::minmax_element(particles.begin(), particles.end(), cmp_y);
	sys::tie(minEl_z, maxEl_z) = sys::minmax_element(particles.begin(), particles.end(), cmp_z);

	min_x = minEl_x->x;
	max_x = maxEl_x->x;
	min_y = minEl_y->y;
	max_y = maxEl_y->y;
	min_z = minEl_x->z;
	max_z = maxEl_x->z;

	double d_x = max_x - min_x;
	double d_y = max_y - min_y;
	double d_z = max_z - min_z;

	// using e.g. thrust::minimum would be a little overpowered here.
	double d_tmp = (d_x > d_y) ? d_x : d_y;
	d = (d_tmp > d_z) ? d_tmp : d_z;

	std::cout << "d: " << d << " Min: (" << min_x << ", " << min_y << ", " << min_z << ")\nMax: (" << max_x << ", " << max_y << ", " << max_z << ")" << std::endl;
	////////////////////////////////////////////////

	// add root Treenode
	tree.clear();
	TreeNode root = {}; // value initialize all members
	// all are initialized to 0 by compiler
	// Add first particle
	root.particles = sys::make_pair(0,1);
	root.min_x = min_x;
	root.min_y = min_y;
	root.min_z = min_z;
	root.max_x = min_x + d;
	root.max_y = min_y + d;
	root.max_z = min_z + d;
	tree.push_back(sys::vector<TreeNode>(1, root));

	std::mutex m_insert, m_move;

	// TODO remove explicit sequentiality
	std::for_each(counting_iterator<int>(1), counting_iterator<int>(N), [&](const int &i) {
		int l = 0;
		TreeNode tn = root;
		Particle p  = particles[i];
		while(true) {
			// if node has subnodes, search in the right subnode

			// if position is empty, add particle
			if (isEmpty(tn)) {
				std::lock_guard<std::mutex> lock(m_insert);
				//If something changed before the lock continue
				if (!isEmpty(tn)) {
					continue;
				}

				// insert particle
				tn.particles.first = i;
				tn.particles.second = i+1;

				// since root contains first particle at minimum
				// every other particle to insert has at least one parent
				TreeNode parent_tn = tree[l-1][tn.parentnode];
				++parent_tn.particles.second;

				double new_min_x, new_max_x, new_min_y, new_max_y, new_min_z, new_max_z;

				// Update boundaries (not set until first element is added
				double half_x = (parent_tn.min_x + parent_tn.max_x)/2;
				double half_y = (parent_tn.min_y + parent_tn.max_y)/2;
				double half_z = (parent_tn.min_z + parent_tn.max_z)/2;

				if (p.x < half_x) {
					new_min_x = parent_tn.min_x;
					new_max_x = half_x;
				} else {
					new_min_x = half_x;
					new_max_x = parent_tn.max_x;
				}
				if(p.y < half_y) {
					new_min_y = parent_tn.min_y;
					new_max_y = half_y;
				} else {
					new_min_y = half_y;
					new_max_y = parent_tn.max_y;
				}
				if(p.z < half_z) {
					new_min_z = parent_tn.min_z;
					new_max_z = half_z;
				} else {
					new_min_z = half_z;
					new_max_z = parent_tn.max_z;
				}

				tn.min_x = new_min_x;
				tn.max_x = new_max_x;
				tn.min_y = new_min_y;
				tn.max_y = new_max_y;
				tn.min_z = new_min_z;
				tn.max_z = new_max_z;
				break;
			}

			if (hasSubnodes(tn)) {
				// find the right subnode, starting from last subnode, because the points are ordered locally
				// if in parallel mode, it's not true for all time.
				//
				// only contains 8 elements, hence use sequential

				// calc next Octant positions
				double half_x = (tn.min_x + tn.max_x)/2;
				double half_y = (tn.min_y + tn.max_y)/2;
				double half_z = (tn.min_z + tn.max_z)/2;

				int k = 0;
				if(p.x < half_x) k += 1;
				if(p.y < half_y) k += 2;
				if(p.z < half_z) k += 4;

				tn = *(tree[l+1].begin()+tn.subnodes.first+k);
				continue; //now search in subnodes
			}

			// else it has to contain only one element
			// thus, divide
			if (hasOneParticle(tn)) {
				std::lock_guard<std::mutex> lock(m_move);
				if (!hasOneParticle(tn)) {
					continue;
				}

				Particle p_old = tn.particles.first;
				// now subdivide node and add old particle at the right position
				double half_x = (parent_tn.min_x + parent_tn.max_x)/2;
				double half_y = (parent_tn.min_y + parent_tn.max_y)/2;
				double half_z = (parent_tn.min_z + parent_tn.max_z)/2;

				int k = 0;
				if(p_old.x < half_x) k += 1;
				if(p_old.y < half_y) k += 2;
				if(p_old.z < half_z) k += 4;

				double new_min_x, new_max_x, new_min_y, new_max_y, new_min_z, new_max_z;
				if (p_old.x < half_x) {
					new_min_x = tn.min_x;
					new_max_x = half_x;
				} else {
					new_min_x = half_x;
					new_max_x = tn.max_x;
				}
				if(p_old.y < half_y) {
					new_min_y = tn.min_y;
					new_max_y = half_y;
				} else {
					new_min_y = half_y;
					new_max_y = tn.max_y;
				}
				if(p_old.z < half_z) {
					new_min_z = tn.min_z;
					new_max_z = half_z;
				} else {
					new_min_z = half_z;
					new_max_z = tn.max_z;
				}

				// get last subnode index from parent. Due to sequentiality, all has to be insertet after that


				tn.subnodes =


				// strict sequential, so change of subnode has no big change





			}

			// TODO throw error, there should be no other possibility
			std::cerr << "Error in building tree!" << std::endl;

		}
	});

}


} // namespace BH



int main(int argc, char *argv[])
{
	std::cout << "CPU BarnesHut v0.01" << std::endl; //TODO use global version definition

	/*  __gnu_parallel::_Settings s;
	  s.algorithm_strategy = __gnu_parallel::force_parallel;
	  __gnu_parallel::_Settings::set(s); */

#ifdef USE_PARALLEL
	std::cout << "Using parallel mode" << std::endl;
#else
	std::cout << "Using sequential mode" << std::endl;
#endif

	if (true/*argc != 3*/) { // TODO change
		std::cerr << "arguments: number_of_bodies number_of_timesteps" << std::endl;
		//exit(-1);
	}




	std::cout << "Generate random entrys" << std::endl;
	BH::generateRandomEntrys();
	//	BH::printParticleData();

	// remove particle data // TODO only when xyz is used
	std::system("rm data.xyz");
	std::cout << "Removed existin data.xyz" << std::endl;

	std::chrono::duration<double> treetime(0), massestime(0), acctime(0), updatetime(0), savetime(0);
	std::chrono::high_resolution_clock::time_point start, stop, start_tmp, stop_tmp;
	if(BH::use_timer)
		start = std::chrono::high_resolution_clock::now();
	for(int step = 0; step < BH::timesteps; ++step) {
		std::cout << "Step " << step << std::endl;
#ifdef INFO_MODE
		std::cout << "Build tree" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp = std::chrono::high_resolution_clock::now();
		BH::buildTree();
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			treetime += stop_tmp - start_tmp ;
		}
		//printTrees();
		//calcMass of branches
		/*
#ifdef INFO_MODE
		std::cout << "Calc node masses" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp  = std::chrono::high_resolution_clock::now();
		BH::calcNodeMasses2();
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			massestime += stop_tmp - start_tmp ;
		}
#ifdef INFO_MODE
		std::cout << "Calc Acc" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp  = std::chrono::high_resolution_clock::now();
		BH::calcAcc(); // TODO make param with lambda expression
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			acctime += stop_tmp - start_tmp ;
		}
#ifdef INFO_MODE
		std::cout << "Update data" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp  = std::chrono::high_resolution_clock::now();
		BH::updateData(); // TODO nur speichern, wenn ein 250 MB groß, o.ä.
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			updatetime += stop_tmp - start_tmp ;
		}


		printParticleData();
#ifdef INFO_MODE
		std::cout << "Save data" << std::endl;
#endif
		if(BH::use_timer)
			start_tmp  = std::chrono::high_resolution_clock::now();
		BH::saveParticleData(step);
		if(BH::use_timer) {
			stop_tmp = std::chrono::high_resolution_clock::now();
			savetime += stop_tmp - start_tmp ;
		}
	}
		 */
		BH::printParticleData();
		//	if(BH::use_timer) {
		//		auto stop = std::chrono::high_resolution_clock::now();
		//		std::chrono::duration<double> duration =  stop - start;
		//
		//		std::cout << "Finished in " << duration.count() << " s" << std::endl;
		//		std::cout << "Tree: " << treetime.count() << " Masses: " << massestime.count() << " Acc: " << acctime.count()
		//								  << " Update: " << updatetime.count() << " Save: " << savetime.count() << std::endl;																																		<< " Update: " << updatetime.count() << " Save: " << savetime.count() << std::endl;
		//	}
	}

	return 0;
}
